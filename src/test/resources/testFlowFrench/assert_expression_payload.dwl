%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "type": "success",
  "message": "The reply Bonjour! means hello in French"
})