{
  "headers": {
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "a6f66395-d8f7-48cc-a968-b9400e92200f",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {
    "lang": "French"
  },
  "requestUri": "/unit_test?lang=French",
  "queryString": "lang=French",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/unit_test",
  "relativePath": "/unit_test",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/unit_test?lang=French",
  "rawRequestPath": "/unit_test",
  "remoteAddress": "/127.0.0.1:54168",
  "requestPath": "/unit_test"
}