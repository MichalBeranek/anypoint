{
  "headers": {
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "a396a819-efed-4116-b8b3-39bc4a7f8f0f",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {
    "lang": "Spanish"
  },
  "requestUri": "/unit_test?lang=Spanish",
  "queryString": "lang=Spanish",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/unit_test",
  "relativePath": "/unit_test",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/unit_test?lang=Spanish",
  "rawRequestPath": "/unit_test",
  "remoteAddress": "/127.0.0.1:54223",
  "requestPath": "/unit_test"
}