%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "type": "success",
  "message": "The reply Hola! means hello in Spanish"
})